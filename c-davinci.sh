DIR_C_DAVINCI="$PWD"/c_davinci
if [ -d $DIR_C_DAVINCI ]; then
    echo -e "\xE2\x9C\x94 c_davinci - El subdirectorio ya existe"
else
    echo -e "\xe2\x9c\x95 c_davinci - No existe el subdirectorio"
    mkdir $DIR_C_DAVINCI
    echo -e "\xE2\x9C\x94 c_davinci - subdirectorio creado"
fi
declare -a extensiones
extensiones[0]="mts"
extensiones[1]="mp4"
extensiones[2]="mov"
for i in "${extensiones[@]}";
do
  echo -e "\xe2\x9d\x82 Buscando archivos $i";
  count=`ls -1 *.$i 2>/dev/null | wc -l`

  if [ $count != 0 ]
  then
      echo -e "\xE2\x9C\x94 $count $i encontrados"
      SAVEIFS=$IFS
      IFS=$(echo -en "\n\b")
      for archivo in `ls *.$i`
        do
            echo -e "\xe2\x9c\x88 Convirtiendo $archivo .||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||.||"
            ffmpeg -i "${archivo}" -vcodec mjpeg -q:v 2 -acodec pcm_s16be -q:a 0 -f mov "${DIR_C_DAVINCI}/${archivo}.mov"
        done
      IFS=$SAVEIFS

  else
    echo -e "\xe2\x9d\x95 $count $i Encontrados"
  fi
done

declare -a extensiones_audio
extensiones_audio[0]="mp3"
extensiones_audio[1]="m4a"
extensiones_audio[2]="aiff"
for i in "${extensiones_audio[@]}";
do
  echo -e "\xe2\x9d\x82 Buscando archivos $i";
  count=`ls -1 *.$i 2>/dev/null | wc -l`

  if [ $count != 0 ]
  then
    echo -e "\xE2\x9C\x94 $count $i encontrados"
    SAVEIFS=$IFS
    IFS=$(echo -en "\n\b")
    for archivo in `ls *.$i`
      do
          echo -e "\xe2\x9c\x88 Convirtiendo $archivo"

          ffmpeg -i "${archivo}" "${DIR_C_DAVINCI}/${archivo}.wav"

      done
    IFS=$SAVEIFS
  else
    echo -e "\xe2\x9d\x95 $count $i Encontrados"
  fi
done
